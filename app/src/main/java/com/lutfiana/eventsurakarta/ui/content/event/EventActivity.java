package com.lutfiana.eventsurakarta.ui.content.event;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lutfiana.eventsurakarta.R;
import com.lutfiana.eventsurakarta.api.ApiClient;
import com.lutfiana.eventsurakarta.api.ApiInterface;
import com.lutfiana.eventsurakarta.api.response.berita.BeritaResponse;
import com.lutfiana.eventsurakarta.api.response.event.EventResponse;
import com.lutfiana.eventsurakarta.ui.content.berita.BeritaActivity;
import com.lutfiana.eventsurakarta.ui.content.berita.BeritaAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventActivity extends AppCompatActivity {
    private RecyclerView rv_event;
    private EventAdapter eventAdapter;
    private List<EventResponse> listApiEvent = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        rv_event = findViewById(R.id.rv_event);
        rv_event.setHasFixedSize(true);

        listEventaApi();
    }

    private void listEventaApi() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<List<EventResponse>> call = apiInterface.getListEvent();
        call.enqueue(new Callback<List<EventResponse>>(){

            @Override
            public void onResponse(Call<List<EventResponse>> call, Response<List<EventResponse>> response) {
                if(response.code()==200){
                    Log.i("RESPONSE", new Gson().toJson(response.body()));
                    listApiEvent = response.body();
                    eventAdapter = new EventAdapter(getApplicationContext(), listApiEvent);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(EventActivity.this);
                    rv_event.setLayoutManager(layoutManager);
                    rv_event.setAdapter(eventAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<EventResponse>> call, Throwable t) {
                Log.d("ERROR RESPONSE : ", t.getMessage());
                Toast.makeText(getApplicationContext(), "Ada Kesalahan", Toast.LENGTH_SHORT).show();
            }
        });
    }
}