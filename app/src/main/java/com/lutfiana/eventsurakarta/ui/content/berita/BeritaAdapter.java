package com.lutfiana.eventsurakarta.ui.content.berita;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lutfiana.eventsurakarta.BuildConfig;
import com.lutfiana.eventsurakarta.R;
import com.lutfiana.eventsurakarta.api.response.berita.BeritaResponse;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.MyViewHolder> {
    private final Context context;
    private final List<BeritaResponse> listBerita;

    public BeritaAdapter(Context context, List<BeritaResponse> listBerita) {
        this.context = context;
        this.listBerita = listBerita;
        listBerita.addAll(listBerita);
        notifyDataSetChanged();
    }

    @NonNull
    @NotNull
    @Override
    public BeritaAdapter.MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BeritaAdapter.MyViewHolder holder, int position) {
        final BeritaResponse data = listBerita.get(position);
        Glide.with(holder.itemView.getContext())
//                .load("http://192.168.0.120/eventska/public/public/beritaupload/" + berita.getGambarBerita())
                .load(BuildConfig.IMAGE_URL + "beritaupload/" +data.getGambarBerita())
                .apply(new RequestOptions().override(100, 100))
                .into(holder.imgBerita);

        holder.tvJudul.setText(data.getJudulBerita());
        holder.tvPenulis.setText(data.getPenulisBerita());
        holder.tvtTgl.setText(data.getTanggalBerita());
    }

    @Override
    public int getItemCount() {
        return listBerita.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgBerita;
        private TextView tvJudul, tvPenulis, tvtTgl;

        public MyViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imgBerita   = itemView.findViewById(R.id.img_berita);
            tvJudul     = itemView.findViewById(R.id.tv_judul);
            tvPenulis   = itemView.findViewById(R.id.tv_ket);
            tvtTgl      = itemView.findViewById(R.id.tv_tgl_item);
        }
    }
}
