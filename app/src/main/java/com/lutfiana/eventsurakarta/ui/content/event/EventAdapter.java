package com.lutfiana.eventsurakarta.ui.content.event;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lutfiana.eventsurakarta.BuildConfig;
import com.lutfiana.eventsurakarta.R;
import com.lutfiana.eventsurakarta.api.response.berita.BeritaResponse;
import com.lutfiana.eventsurakarta.api.response.event.EventResponse;
import com.lutfiana.eventsurakarta.ui.content.berita.BeritaAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder>{
    private final Context context;
    private final List<EventResponse> listEvent;

    public EventAdapter(Context context, List<EventResponse> listEvent) {
        this.context = context;
        this.listEvent = listEvent;
        listEvent.addAll(listEvent);
        notifyDataSetChanged();
    }

    @NonNull
    @NotNull
    @Override
    public EventAdapter.MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder holder, int position) {
        final EventResponse data = listEvent.get(position);
        Glide.with(holder.itemView.getContext())
//                .load("http://192.168.0.120/eventska/public/public/beritaupload/" + berita.getGambarBerita())
                .load(BuildConfig.IMAGE_URL + "eventupload/" +data.getGambarEvent())
                .apply(new RequestOptions().override(100, 100))
                .into(holder.imgEvent);
        holder.tvTgl.setText(data.getTanggalEvent());
        holder.tvJudul.setText(data.getJudulEvent());
        holder.tvTempat.setText(data.getTempatEvent());
    }

    @Override
    public int getItemCount() {
        return listEvent.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgEvent;
        private TextView tvJudul, tvTempat, tvTgl;

        public MyViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imgEvent   = itemView.findViewById(R.id.img_berita);
            tvJudul     = itemView.findViewById(R.id.tv_judul);
            tvTempat   = itemView.findViewById(R.id.tv_ket);
            tvTempat   = itemView.findViewById(R.id.tv_tgl_item);
        }
    }
}
