package com.lutfiana.eventsurakarta.ui.content.berita;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lutfiana.eventsurakarta.R;
import com.lutfiana.eventsurakarta.api.ApiClient;
import com.lutfiana.eventsurakarta.api.ApiInterface;
import com.lutfiana.eventsurakarta.api.response.berita.BeritaResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeritaActivity extends AppCompatActivity {
    private RecyclerView rv_berita;
    private BeritaAdapter beritaAdapter;
    private List<BeritaResponse> listApiBerita = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita);

        rv_berita = findViewById(R.id.rv_berita);
        rv_berita.setHasFixedSize(true);

        listBeritaApi();
    }

    private void listBeritaApi(){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<List<BeritaResponse>> call = apiInterface.getListBerita();

        call.enqueue(new Callback<List<BeritaResponse>>() {
            @Override
            public void onResponse(Call<List<BeritaResponse>> call, Response<List<BeritaResponse>> response) {
                if(response.code()==200){
                    Log.i("RESPONSE", new Gson().toJson(response.body()));
                    listApiBerita = response.body();
                    beritaAdapter = new BeritaAdapter(getApplicationContext(), listApiBerita);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(BeritaActivity.this);
                    rv_berita.setLayoutManager(layoutManager);
                    rv_berita.setAdapter(beritaAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<BeritaResponse>> call, Throwable t) {
                Log.d("ERROR RESPONSE : ", t.getMessage());
                Toast.makeText(getApplicationContext(), "Ada Kesalahan", Toast.LENGTH_SHORT).show();
            }
        });
    }
}