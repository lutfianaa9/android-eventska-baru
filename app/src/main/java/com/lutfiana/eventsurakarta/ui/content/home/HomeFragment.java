package com.lutfiana.eventsurakarta.ui.content.home;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.lutfiana.eventsurakarta.R;
import com.lutfiana.eventsurakarta.ui.content.berita.BeritaActivity;
import com.lutfiana.eventsurakarta.ui.content.event.EventActivity;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private CardView cvBerita, cvEvent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        cvBerita = view.findViewById(R.id.cv_berita);
        cvEvent = view.findViewById(R.id.cv_event);
        cvBerita.setOnClickListener(this);
        cvEvent.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.cv_berita:
                Intent br = new Intent(getContext(), BeritaActivity.class);
                startActivity(br);
                break;
            case R.id.cv_event:
                Intent ev = new Intent(getContext(), EventActivity.class);
                startActivity(ev);
                break;
        }
    }
}