package com.lutfiana.eventsurakarta.api;

import com.lutfiana.eventsurakarta.api.response.berita.BeritaResponse;
import com.lutfiana.eventsurakarta.api.response.event.EventResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("show_berita")
    Call<List<BeritaResponse>> getListBerita();

    @GET("show_event")
    Call<List<EventResponse>> getListEvent();
}
