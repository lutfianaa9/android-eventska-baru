package com.lutfiana.eventsurakarta.api.response.berita;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BeritaResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("judul_berita")
    @Expose
    private String judulBerita;
    @SerializedName("penulis_berita")
    @Expose
    private String penulisBerita;
    @SerializedName("isi_berita")
    @Expose
    private String isiBerita;
    @SerializedName("gambar_berita")
    @Expose
    private String gambarBerita;
    @SerializedName("tanggal_berita")
    @Expose
    private String tanggalBerita;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJudulBerita() {
        return judulBerita;
    }

    public void setJudulBerita(String judulBerita) {
        this.judulBerita = judulBerita;
    }

    public String getPenulisBerita() {
        return penulisBerita;
    }

    public void setPenulisBerita(String penulisBerita) {
        this.penulisBerita = penulisBerita;
    }

    public String getIsiBerita() {
        return isiBerita;
    }

    public void setIsiBerita(String isiBerita) {
        this.isiBerita = isiBerita;
    }

    public String getGambarBerita() {
        return gambarBerita;
    }

    public void setGambarBerita(String gambarBerita) {
        this.gambarBerita = gambarBerita;
    }

    public String getTanggalBerita() {
        return tanggalBerita;
    }

    public void setTanggalBerita(String tanggalBerita) {
        this.tanggalBerita = tanggalBerita;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
