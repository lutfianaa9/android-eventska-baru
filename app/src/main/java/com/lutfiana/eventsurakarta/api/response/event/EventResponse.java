package com.lutfiana.eventsurakarta.api.response.event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventResponse {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("judul_event")
    @Expose
    private String judulEvent;
    @SerializedName("tempat_event")
    @Expose
    private String tempatEvent;
    @SerializedName("tanggal_event")
    @Expose
    private String tanggalEvent;
    @SerializedName("waktu_event")
    @Expose
    private String waktuEvent;
    @SerializedName("gambar_event")
    @Expose
    private String gambarEvent;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("tata_cara")
    @Expose
    private String tataCara;
    @SerializedName("ringkasan")
    @Expose
    private String ringkasan;
    @SerializedName("maps")
    @Expose
    private String maps;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudulEvent() {
        return judulEvent;
    }

    public void setJudulEvent(String judulEvent) {
        this.judulEvent = judulEvent;
    }

    public String getTempatEvent() {
        return tempatEvent;
    }

    public void setTempatEvent(String tempatEvent) {
        this.tempatEvent = tempatEvent;
    }

    public String getTanggalEvent() {
        return tanggalEvent;
    }

    public void setTanggalEvent(String tanggalEvent) {
        this.tanggalEvent = tanggalEvent;
    }

    public String getWaktuEvent() {
        return waktuEvent;
    }

    public void setWaktuEvent(String waktuEvent) {
        this.waktuEvent = waktuEvent;
    }

    public String getGambarEvent() {
        return gambarEvent;
    }

    public void setGambarEvent(String gambarEvent) {
        this.gambarEvent = gambarEvent;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTataCara() {
        return tataCara;
    }

    public void setTataCara(String tataCara) {
        this.tataCara = tataCara;
    }

    public String getRingkasan() {
        return ringkasan;
    }

    public void setRingkasan(String ringkasan) {
        this.ringkasan = ringkasan;
    }

    public String getMaps() {
        return maps;
    }

    public void setMaps(String maps) {
        this.maps = maps;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
